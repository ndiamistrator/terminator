try:
    import getpass
    import socket

    user = getpass.getuser()
    host = socket.gethostname()

    if user and host:
        userhost = f'{user}@{host}'
        userhost1 = f'{userhost}:'
        userhost2 = f'{userhost}: '
    else:
        userhost = None

except Exception as E:
    import warnings
    warnings.warn(f'cannot get username / hostname: {E}')

    userhost = None


def remove_userhost(title):
    if userhost:
        return title.replace(userhost2, '').replace(userhost1, '').replace(userhost, '').strip()
    else:
        return title.strip()
